import Pkg
Pkg.update()
Pkg.add("AverageShiftedHistograms")
Pkg.add("Distributions")
Pkg.add("IJulia")
Pkg.add("ImageView")
Pkg.add("Images")
Pkg.add("Interact")
Pkg.add("Interpolations")
Pkg.add("LaTeXStrings")
Pkg.add("MultivariateStats")
Pkg.add("NearestNeighbors")
Pkg.add("Plotly")
Pkg.add("PlotlyJS")
Pkg.add("PyCall")
Pkg.add("PyPlot")
Pkg.add("Plots")
Pkg.add("QuartzImageIO")
Pkg.add("StatsPlots")
Pkg.add("StatsBase")
Pkg.add("SymPy")
Pkg.add("TaylorSeries")
# Update packages again needed
Pkg.update()
# Precompile all packages except ImageView because it requires DISPLAY to be set \
Pkg.precompile(["AverageShiftedHistograms", "Distributions", "IJulia", "Images", "Interact", 
    "Interpolations", "LaTeXStrings", "MultivariateStats", "NearestNeighbors", "Plotly", "PlotlyJS",
    "PyCall", "PyPlot", "Plots", "QuartzImageIO", "StatsPlots", "StatsBase", "SymPy", "TaylorSeries"])
