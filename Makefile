.PHONY: help build tag push

TAG := $(shell git describe --always)
CI_REGISTRY_IMAGE := registry.esss.lu.se/ics-docker/notebook
CURDIR := $(shell pwd)

help:
	@echo "Please use \`make <target>' where <target> is one of"
	@echo "  build       to build the Docker images"
	@echo "  test        to run the tests"
	@echo "  run         to run jupyterlab"

build:
	docker build -t base:latest base-notebook
	docker build -t python:latest python-notebook
	docker build -t julia:latest julia-notebook
	docker build -t $(CI_REGISTRY_IMAGE):latest openxal-notebook

test:
	docker run --rm -it -v $(CURDIR)/notebooks:/home/conda/notebooks $(CI_REGISTRY_IMAGE):latest /opt/conda/envs/jupyter/bin/py.test --nbval jupyter/
	docker run --rm -it -v $(CURDIR)/notebooks:/home/conda/notebooks $(CI_REGISTRY_IMAGE):latest /opt/conda/envs/mantid/bin/py.test --nbval mantid/
	docker run --rm -it -v $(CURDIR)/notebooks:/home/conda/notebooks $(CI_REGISTRY_IMAGE):latest /opt/conda/envs/jupyter/bin/py.test --nbval julia/
	docker run --rm -it -v $(CURDIR)/notebooks:/home/conda/notebooks $(CI_REGISTRY_IMAGE):latest /opt/conda/envs/jupyter/bin/jupyter nbconvert --to pdf jupyter/altair-demo.ipynb

run:
	docker run --rm -it -p 8888:8888 -v $(CURDIR)/notebooks:/home/conda/notebooks $(CI_REGISTRY_IMAGE):latest
