# Configuration file for jupyter-notebook in standalone mode.
# File is not named jupyter_notebook_config.py so that
# it is not read when using jupyterhub.
# It shall be passed as argument to be used.

## (bytes/sec) Maximum rate at which messages can be sent on iopub before they
#  are limited.
c.ServerApp.iopub_data_rate_limit = 0

## The IP address the notebook server will listen on.
c.ServerApp.ip = '0.0.0.0'

## The directory to use for notebooks and kernels.
c.ServerApp.root_dir = '/home/conda/notebooks'

## GitLab url used by the jupyterlab-gitlab server extension
c.GitLabConfig.url = "https://gitlab.esss.lu.se"

## Whether to open in a browser after starting. The specific browser used is
#  platform dependent and determined by the python standard library `webbrowser`
#  module, unless it is overridden using the --browser (NotebookApp.browser)
#  configuration option.
c.NotebookApp.open_browser = False

## The port the notebook server will listen on.
c.ServerApp.port = 8888
