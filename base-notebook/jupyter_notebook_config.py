# Default configuration file for notebook used by JupyterHub.

## (bytes/sec) Maximum rate at which messages can be sent on iopub before they
#  are limited.
c.ServerApp.iopub_data_rate_limit = 0

## GitLab url used by the jupyterlab-gitlab server extension
c.GitLabConfig.url = "https://gitlab.esss.lu.se"

# Adding 
c.ServerApp.contents_manager_class = "jupytext.TextFileContentsManager"
